class Vacuna():
    def nombres():
        print("------------------------")
        print("SELECCIONE LA VACUNA")
        print("PFIZER  [1]")
        print("SINOVAC [2]")
        print("CANSINO [3]")
        valor = int(input("Seleccione una vacuna: "))
        if valor != 1 and valor != 2 and valor != 3:
                print("Numero no valido, ingrese nuevamente")
                valor = int(input("Seleccione una vacuna: "))
        return valor

    def lab(valor):
        if valor == 1:
            nombre = "Pfizer"
            lab = "Laboratorio CHILE"
        elif valor == 2:
            nombre = "Sinovac"
            lab = "Laboratorio ARGENTINA"
        else:
            nombre = "Cansino"
            lab = "Laboratorio BRASIL"
        return nombre, lab
    
    def efectos_secundarios(nombre):
        if nombre == "Pfizer":
            e_pfizer = ["dolor de cabeza", "vomito"]
            efecto_vacuna = e_pfizer
        elif nombre == "Sinovac":
            e_sinovac = ["secrecion nasal", "sed excesiva"]
            efecto_vacuna = e_sinovac
        else:
            e_cansino = ["dolor de cuerpo", "tos"]
            efecto_vacuna = e_cansino
        return efecto_vacuna
    
    def set_agrega_efecto_secundario(efecto_vacuna):
        efecto = input("Ingrese un nuevo efecto secundario: ")
        efecto_vacuna.append(efecto)
        print("El efecto se añadio correctamente")

    def mostrar_efectos(nombre,lab,efecto_vacuna):
        print("------------------------")
        print("VACUNA:",nombre)
        print("LABORATORIO:",lab)
        print("EFECTOS SECUNDARIOS:")
        for efecto in efecto_vacuna:
            print("-", efecto)

    def menu():
        print("MENU")
        print("[1] MOSTRAR EFECTOS SECUNDARIOS")
        print("[2] AÑADIR EFECTOS SECUNDARIOS")
        opcion = int(input("seleccione una opcion:"))
        return opcion

    opcion = menu()

    valor = nombres()
    nombre, lab = lab(valor)
    efecto_vacuna = efectos_secundarios(nombre)
    if opcion == 1:
        mostrar_efectos(nombre,lab,efecto_vacuna)
    else:
        set_agrega_efecto_secundario(efecto_vacuna)
        mostrar_efectos(nombre,lab,efecto_vacuna)
    
