class sistema():
    def planetas():
        print("------------------------")
        print("SELECCIONE UN PLANETA")
        print("VENUS  [1]")
        print("MARTE  [2]")
        print("TIERRA [3]")
        valor = int(input("Seleccione un planeta: "))
        if valor != 1 and valor != 2 and valor != 3:
                print("Numero no valido, ingrese nuevamente")
                valor = int(input("Seleccione un planeta: "))
        if valor == 1:
            nombre = "VENUS"
        elif valor == 2:
            nombre = "MARTE"
        else:
            nombre = "TIERRA"
        return nombre


    def datos_planetas(nombre):
        if nombre == "VENUS":
            info_venus = ["400000000000 kg", "5.24 g/cm3","12103 km", "3 años luz", "001","VENUS"]
            informacion = info_venus
        elif nombre == "MARTE":
            info_marte = ["2389283923293 kg", "6.24 g/cm3","15450450 km", "10 años luz", "002","MARTE"]
            informacion = info_marte
        else:
            info_tierra = ["938439483994 kg", "10.75 g/cm3","50450485 km", "15 años luz", "003","TIERRA"]
            informacion = info_tierra
        return informacion

    def imprimir(informacion):
        print("------------------------")
        print("MASA:", informacion[0])
        print("DENSIDAD:",informacion[1])
        print("DIAMETRO:",informacion[2])
        print("DISTANCIA AL SOL:",informacion[3])
        print("ID UNICO:",informacion[4])
        print("NOMBRE:",informacion[5])


    nombre = planetas()
    informacion = datos_planetas(nombre)
    imprimir(informacion)
